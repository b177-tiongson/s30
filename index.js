const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

//MongoDB connection
mongoose.connect("mongodb+srv://admin:CELxOfPENWdbDF3P@wdc028-course-booking.dhurq.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
})

//set notification for connection success or failure
//Connection to the Database
let db = mongoose.connection;

// If a connection error occurred, output a message in the console.
db.on("error", console.error.bind(console, "Connection error"));

//if the connection is successfull, output a message in the console.

db.once("open",()=> console.log("We're connected to the cloud database"))

//Create a Task schema
const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field
		default: "pending"
	}
})

//Create Models
//Server > Schema > Database > Collection(MongoDB)
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

//Create a POST route to create a new task
app.post("/tasks", (req, res)=>{
	Task.findOne({name: req.body.name}, (err,result)=>{
		if(result != null && result.name ==req.body.name){
			//returns a message to the client/postman
			return res.send("Duplicate task found")
		}
		//if no document found
		else{
			//create a new task and save it to the database.
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask)=>{
				//if there are errors in saving
				if(saveErr){
					return console.log.error(saveErr)
				}
				//if no error found while creating the document
				else{
					return res.status(201).send("New task created")

				}

			})

		}
	})
})

//create a GET request to retrieve all the task

app.get("/tasks", (req,res)=>{
	Task.find({},(err,result)=> {
		//if an error occured
		if(err){
			//will print any errors found in the console.
			return console.log(err)
		}
		//if no errors found
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

//--------------ACTIVITY--------------

// Create a User schema.
const userSchema = new mongoose.Schema({

	username: String,
	password: String
	
})


// Create a User model.
const User = mongoose.model("User", userSchema)


// Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup", (req, res)=>{


	User.findOne({username: req.body.username, password: req.body.password}, (err,result)=>{


		if(req.body.username == null || req.body.password == null){

			return res.send("Please insert a username and password")
		}

		else if(result != null && result.username == req.body.username){
			//returns a message to the client/postman
			return res.send("Username already existing.")
		}
		//if no document found
		else{
			//create a new task and save it to the database.
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedTask)=>{
				//if there are errors in saving
				if(saveErr){
					return console.log.error(saveErr)
				}
				//if no error found while creating the document
				else{
					return res.status(201).send("New user created")

				}

			})

		}
	})
})


app.get("/users", (req,res)=>{
	User.find({},(err,result)=> {
		//if an error occured
		if(err){
			//will print any errors found in the console.
			return console.log(err)
		}
		//if no errors found
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})


app.listen(port, ()=>console.log(`Server running at port ${port}`));